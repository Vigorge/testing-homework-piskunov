package account

case class Account private (balance: Int) {
  def put(ammount: Int): Account =
    if (ammount > 0)
      new Account(balance + ammount)
    else this

  def withdraw(ammount: Int): Account =
    if (ammount > 0 && ammount <= balance)
      new Account(balance - ammount)
    else this

  def checkBalance: Int = balance
}

object Account {
  def open(ballance: Int): Option[Account] = ballance match {
    case robbing if robbing < 0 => None
    case putting => Some(new Account(putting))
  }
}
