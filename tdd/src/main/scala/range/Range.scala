package range

sealed trait Range {
  def size : Int = this match {
    case IntegralRange(l, r) => r - l + 1
    case EmptyRange => 0
  }

  def intersection(other: Range): Range = (this, other) match {
    case (IntegralRange(tl, tr), IntegralRange(ol, or)) =>
      val newLeft = math.max(tl, ol)
      val newRight = math.min(tr, or)
      if (newRight >= newLeft) IntegralRange(newLeft, newRight) else EmptyRange
    case _ => EmptyRange
  }

  def union(other: Range): Range = (this, other) match {
    case (IntegralRange(tl, tr), IntegralRange(ol, or)) =>
      val newLeft = math.min(tl, ol)
      val newRight = math.max(tr, or)
      if (newRight - newLeft + 1 <= this.size + other.size - 1 ) IntegralRange(newLeft, newRight) else EmptyRange
    case (intR@IntegralRange(_, _), EmptyRange) => intR
    case (EmptyRange, intR@IntegralRange(_, _)) => intR
    case _ => EmptyRange
  }

  def isEmpty: Boolean = this match {
    case EmptyRange => true
    case _ => false
  }

  def isIn(dot: Int): Boolean = this match {
    case IntegralRange(left, right) =>
      if (left <= dot && dot <= right) true else false
    case EmptyRange => false
  }

  def equals(other: Range): Boolean = (this, other) match {
    case (IntegralRange(tl, tr), IntegralRange(ol, or)) =>
      return tl == ol && tr == or
    case (EmptyRange, EmptyRange) => true
    case _ => false
  }

  def intersect(other: Range): Boolean = !(this intersection other).isEmpty

  def isSubOf(other: Range): Boolean = (other union this) == other

  def min: Option[Int] = this match {
    case IntegralRange(left, _) => Some(left)
    case EmptyRange => None
  }

  def max: Option[Int] = this match {
    case IntegralRange(_, right) => Some(right)
    case EmptyRange => None
  }

  def iterate(step: Int = 1): Iterator[Int] = this match {
    case EmptyRange => Iterator.empty
    case IntegralRange(left, right) => new Iterator[Int] {
      private[this] var nowOn = if (step >= 0) left else right

      override def hasNext: Boolean = step match {
        case 0 => false
        case pos if pos > 0 => nowOn <= right
        case neg if neg < 0 => nowOn >= left
      }

      override def next(): Int =
        if (hasNext) {
          val value = nowOn
          nowOn += step
          value
        }
        else Iterator.empty.next()
    }
  }

}

object Range {
  def empty: Range = EmptyRange

  def apply(left: Int, right: Int): Range =
    if (left <= right) IntegralRange(left, right) else EmptyRange

  implicit class RangeSyntax(val a: Range) extends AnyVal {
    def &(b: Range): Range = a.intersection(b)

    def |(b: Range): Range = a.union(b)

    def unary_~(): Boolean = a.isEmpty

    def ==(b: Range): Boolean = a.equals(b)

  }

  implicit class IntwithRangeSyntax(val a: Int) extends AnyVal {
    def ≈(r: Range): Boolean = r.isIn(a)
  }
}

private case class IntegralRange(left: Int, right: Int) extends Range {
  override def toString: String = s"[$left, $right]"

}

private case object EmptyRange extends Range {
  override def toString: String = "[Ø]"
}
