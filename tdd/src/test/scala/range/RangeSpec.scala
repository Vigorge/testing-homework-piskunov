package range

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import range.Range._

class RangeSpec extends AnyFlatSpec with Matchers {
  "&" should "возвращать пересечение двух интервалов, если они пересекаются" in {
    Range(2, 5) & Range(4, 8) should be(
      Range(4, 5)
    )

    Range(2, 5) & Range(6, 8) should be(
      Range.empty
    )

    Range(2, 5) & Range.empty should be(
      Range.empty
    )

    Range.empty & Range(6, 8) should be(
      Range.empty
    )
  }

  it should "возвращать пустой интервал, если интервалы не пересекаются" in {
    Range(2, 5) & Range(6, 8) should be(
      Range.empty
    )
  }

  it should "возвращать пустой интервал, если один из интервалов пустой" in {
    Range(2, 5) & Range(4, 8) should be(
      Range(4, 5)
    )

    Range(2, 5) & Range(6, 8) should be(
      Range.empty
    )

    Range(2, 5) & Range.empty should be(
      Range.empty
    )

    Range.empty & Range(6, 8) should be(
      Range.empty
    )
  }

  "|" should "возвращать объединение двух интервалов, если они пересекаются" in {
    Range(2, 5) | Range(4, 8) should be(
      Range(2, 8)
    )
  }

  it should "возвращать пустой интервал, если интервалы не пересекаются" in {
    Range(2, 5) | Range(6, 8) should be(
      Range.empty
    )
  }

  it should "возвращать один из двух интервалов, если второй пустой" in {
    Range(2, 5) | Range.empty should be(
      Range(2, 5)
    )

    Range.empty | Range(6, 8) should be(
      Range(6, 8)
    )
  }

  "~" should "возвращать true, если интервал пустой" in {
    ~Range.empty should be(true)
  }

  it should "возвращать false, если интервал не пустой" in {
    ~Range(4, 6) should be(false)
  }

  "≈" should "возвращать true, если число входит в интервал" in {
    2 ≈ Range(1, 4) should be(true)
  }

  it should "возвращать false, если число не входит в интервал или интервал пустой" in {
    2 ≈ Range(4, 6) should be(false)

    2 ≈ Range.empty should be(false)
  }

  "==" should "возвращать true, если интервалы совпадают" in {
    Range(1, 4) == Range(1, 4) should be(true)

    Range.empty == Range.empty should be(true)
  }

  it should "возвращать false, если интервалы не совпадают" in {
    Range(1, 4) == Range(3, 4) should be(false)

    Range(3, 5) == Range(3, 4) should be(false)

    Range.empty == Range(2, 2) should be(false)

    Range(2, 2) == Range.empty should be(false)
  }

  "intersect" should "возвращать true, если интервалы пересекаются" in {
    Range(1, 4) intersect Range(1, 4) should be(true)
  }

  it should "возвращать false, если интервалы не пересекаются" in {
    Range(1, 4) intersect Range(7, 10) should be(false)

    Range.empty intersect Range(2, 2) should be(false)

    Range(2, 2) intersect Range.empty should be(false)
  }

  "isSubOf" should "возвращать true, если интервал входит в другой" in {
    Range(2, 4) isSubOf  Range(1, 4) should be(true)

    Range.empty isSubOf Range(2, 2) should be(true)

    Range.empty isSubOf Range.empty should be(true)
  }

  it should "возвращать false, если интервал не входит в другой" in {
    Range(1, 4) isSubOf Range(7, 10) should be(false)

    Range(1, 2) isSubOf Range(2, 3) should be(false)

    Range(2, 2) isSubOf Range.empty should be(false)
  }

  "min" should "возвращать левую границу интервала, если интервал не пустой" in {
    Range(2, 4).min should be(Some(2))
  }

  it should "возвращать None, если интервал пустой" in {
    Range.empty.min should be(None)
  }

  "max" should "возвращать правую границу интервала, если интервал не пустой" in {
    Range(2, 4).max should be(Some(4))
  }

  it should "возвращать None, если интервал пустой" in {
    Range.empty.max should be(None)
  }

  "iterate" should "возвращать iterator по элементам интервала с шагом step" in {
    Range(2, 4).iterate().toList should be(List(2, 3, 4))

    Range(2, 4).iterate(2).toList should be(List(2, 4))

    Range(2, 4).iterate(3).toList should be(List(2))

    Range(2, 4).iterate(-1).toList should be(List(4, 3, 2))

    Range(2, 4).iterate(0).toList should be(List.empty)

    Range.empty.iterate().toList should be(List.empty)
  }

  "toString" should "возвращать строковое представление интервала" in {
    Range(2, 4).toString should be("[2, 4]")

    Range.empty.toString should be("[Ø]")
  }

}
