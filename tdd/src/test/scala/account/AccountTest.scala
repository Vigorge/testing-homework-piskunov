package account

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AccountTest extends AnyFlatSpec with Matchers {

  "put" should "возвращать счёт с обновлённым балансом, если кладётся положительная сумма" in {
    acc500.put(100) should be(acc600)
  }

  it should "возвращать не изменённый счёт, если кладётся не положительная сумма" in {
    acc500.put(-100) should be(acc500)

    acc500.put(0) should be(acc500)
  }

  "withdraw" should "возвращать счёт с обновлённым балансом, если снимаемая сумма не превышает баланс" in {
    acc500.withdraw(100) should be(acc400)
  }

  it should "возвращать не изменённый счёт, если снимаемая сумма превышает баланс или отрицательная" in {
    acc500.withdraw(-100) should be(acc500)

    acc500.withdraw(1000) should be(acc500)
  }

  "open" should "возвращать новый счёт, если исходный баланс положительный" in {
    Account.open(400).get.checkBalance should be(400)
  }

  it should "возвращать None, если исходный баланс отрицательный" in {
    Account.open(-400) should be(None)
  }
  
  private val acc400 = Account.open(400).get
  private val acc500 = Account.open(500).get
  private val acc600 = Account.open(600).get

}
